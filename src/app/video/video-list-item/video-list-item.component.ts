import { Component, OnInit, Input } from '@angular/core';
import { Video } from '../video';
@Component({
  selector: 'video-list-item',
  templateUrl: './video-list-item.component.html',
  styleUrls: ['./video-list-item.component.sass']
})
export class VideoListItemComponent implements OnInit {
  @Input()
  video: Video;

  constructor() {
    this.video = new Video;
  }

  ngOnInit() {
  }

}
