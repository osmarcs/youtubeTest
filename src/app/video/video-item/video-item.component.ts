import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Video } from '../video';

@Component({
  selector: 'video-item',
  templateUrl: './video-item.component.html',
  styleUrls: ['./video-item.component.sass']
})
export class VideoItemComponent implements OnInit {

  @Input()
  videoShowed:Video;
  @Input()
  autoplay:Boolean;
  private lastUrl:SafeResourceUrl;
  private lastId:String;
  active:Boolean;
  constructor(private sanitizer: DomSanitizer) {
    this.videoShowed = new Video;
  }

  ngOnInit() { }

  doVideoUrl() {
    if (this.videoShowed.id === this.lastId) {
      return this.lastUrl;
    }
    let url = `https://www.youtube.com/embed/${this.videoShowed.id}?autoplay=${this.autoplay}`;
    this.lastUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
    this.lastId  = this.videoShowed.id;
    return this.lastUrl;
  }

  toggleDescription() {
    this.active = !this.active;
  }

}
