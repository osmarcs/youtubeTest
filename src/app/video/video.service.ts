import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Video } from './video';
import { ListVideos } from './list-videos';
import { YoutubeConstants } from './YoutubeConstants';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

@Injectable()
export class VideoService {
  private termSearch:String;

  constructor(private http:Http) { }

  getVideos(termSearch:String="", page:String=""): Observable<ListVideos> {
    let url = `${YoutubeConstants.url}${YoutubeConstants.endpoint.SEARCH}?key=${YoutubeConstants.key}&channelId=${YoutubeConstants.channelId}&part=snippet&q=${termSearch}&order=date&maxResults=${YoutubeConstants.maxResults}&pageToken=${page}&type=video`;
    this.termSearch = termSearch;

    return this.http.get(url)
    .map(res  => res.json())
    .map(data => this.transformData(data))
  }

  loadMore(nextPage) {
    this.getVideos(this.termSearch, nextPage)
  }

  private transformData(data): ListVideos {
    let items = data.items || [];
    let videos = items.map(function (item) {
      let video = <Video>{
        id: item.id.videoId,
        published: new Date(item.snippet.publishedAt),
        title: item.snippet.title,
        thumbs:  {
          default: item.snippet.thumbnails.default.url,
          medium: item.snippet.thumbnails.medium.url,
          high: item.snippet.thumbnails.high.url,
        }
      }
      return video;
    });
    let listVideos = new ListVideos;

    if (videos.length == 0) {
      listVideos.nextPage = null
      listVideos.finished = true;
      return listVideos;
    }
    this.loadStatistics(videos);
    listVideos.nextPage = data.nextPageToken;
    listVideos.videos = videos;
    return listVideos;
  }

  private loadStatistics(videos) {
    if (!videos.length) {
      return;
    }
    let videosId = videos.reduce(function (reduceId, video) {
      if (typeof reduceId !== 'string') {
        reduceId = reduceId.id;
      }
      return `${reduceId},${video.id}`;
    });
    let url = `${YoutubeConstants.url}${YoutubeConstants.endpoint.VIDEOS}?key=${YoutubeConstants.key}&part=snippet,statistics&id=${videosId}`;

    return this.http.get(url)
    .map(res => res.json())
    .map(this.retriveVideosCountView)
    .subscribe(
      videosStatistics => {
        videos.forEach(function (video) {
          video.viewCount   = videosStatistics[video.id].viewCount;
          video.description = videosStatistics[video.id].description;
        })
      }
    )

  }


  private retriveVideosCountView(data) {
    let items = data.items || [];
    let videosStatistics = {}
    items.forEach(function (item) {
      videosStatistics[item.id] ={
        viewCount: item.statistics.viewCount,
        description: item.snippet.description,
      };
    });
    return videosStatistics;
  }

}
