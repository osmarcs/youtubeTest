import { Video } from './video';
export class ListVideos {
  public nextPage: String;
  public videos: Video[];
  public loading: Boolean;
  public finished: Boolean;

  constructor() {
    this.videos = [];
  }
}
