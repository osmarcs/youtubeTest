import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'countK'
})
export class CountKPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let dec = args || 1
    return this.countK(value, dec);
  }

  private countK (number:number,dec:number=1) {
    var aux=(''+number).length;
    dec = Math.pow(10,dec);
    aux-=aux%3;
    return Math.round(number*dec/Math.pow(10,aux))/dec+" kMBTTQ"[aux/3]
  }

}
