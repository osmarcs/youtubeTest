import { Video } from './../video';
import { ListVideos } from './../list-videos';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

export class MockVideoService {
  getVideos(termSearch:String="", page:String=""): Observable<ListVideos> {
    let listVideos = new ListVideos;
    let until = 5;

    if (termSearch) {
      until = 3
    }

    if (page == 'lastPage') {
      until = 2
    }

    if (page == 'noResults') {
      until = 0
    }

    for (let i = 0; i < until; i++) {
      let video = <Video> {
        id: "##FD"+i,
        published: new Date,
        title: 'title '+i,
        description: "description",
        thumbs: {
          default: 'default',
          medium: 'medium',
          high: 'high',
        },
        viewCount: 1234
      }

      listVideos.videos.push(video);
    }

    return Observable.of(listVideos);

  }
}
