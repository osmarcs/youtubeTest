export class YoutubeConstants {
  public static url = 'https://www.googleapis.com/youtube/v3/';
  public static key = 'AIzaSyAZa7NwoqnkqxrSYCJMmpY2Gdp5AAeGY9M';
  public static channelId = 'UCH2VZQBLFTOp6I_qgnBJCuQ';
  public static maxResults = 4;
  public static endpoint = {
    VIDEOS: 'videos',
    SEARCH: 'search'
  }
}
