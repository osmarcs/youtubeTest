export class Video {
  public id:String;
  public published:Date;
  public title:String;
  public description:String;
  public thumbs:{
    default:String,
    medium:String,
    high:String
  }
  public viewCount: number;

  constructor() {
    this.thumbs = {
      default: "",
      medium: "",
      high: ""
    };
  }
}
