import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoListItemComponent } from './video-list-item/video-list-item.component';
import { VideoService } from './video.service';
import { CountKPipe } from './count-k.pipe';
import { VideoItemComponent } from './video-item/video-item.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [VideoListItemComponent, CountKPipe, VideoItemComponent],
  exports: [VideoListItemComponent, CountKPipe, VideoItemComponent],
  providers: [VideoService, { provide: LOCALE_ID, useValue: "pt-BR" }]
})
export class VideoModule { }
