import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'menu-xpto',
  templateUrl: './menu-xpto.component.html',
  styleUrls: ['./menu-xpto.component.sass']
})
export class MenuXptoComponent implements OnInit {

  @Input()
  items: [{
    title:String,
    url:String
    icon:String
  }]
  constructor() { }

  ngOnInit() {
  }

}
