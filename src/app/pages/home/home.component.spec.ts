import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';

import { HomeComponent } from './home.component';
import { VideoModule } from './../../video/video.module';
import { MockVideoService } from './../../video/mock/mock.videos.service';
import { VideoService } from './../../video/video.service';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        VideoModule
      ],
      declarations: [ HomeComponent ]
    }).overrideComponent(HomeComponent, {
      set: {
        providers: [
          {provide: VideoService, useClass: MockVideoService}
        ]
      }
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should have item videos in first load', async(() => {
    expect(component.listVideos.videos).not.toBeUndefined();
    expect(component.listVideos.videos.length).toBeGreaterThan(0);
  }));

  it('should have video-list-item', async(() => {
    expect(component.listVideos.videos).not.toBeUndefined();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelectorAll('video-list-item').length).toBeGreaterThan(0);
  }));

  it('should have video-item ', async(() => {
    expect(component.listVideos.videos).not.toBeUndefined();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelectorAll('video-item').length).toEqual(1);
  }));

  it('should click video-list-item and title match with video-item.title', async(() => {
    expect(component.listVideos.videos).not.toBeUndefined();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;

    let  video = compiled.querySelectorAll('video-list-item')[3].click();
    fixture.detectChanges();

    let videoItemH2 =  compiled.querySelector('video-item').querySelector('h3');
    expect(videoItemH2.textContent).toContain("title 3");
  }));

});
