import { Component, OnInit } from '@angular/core';
import { VideoService } from '../../video/video.service';
import { ListVideos } from '../../video/list-videos';
import { Video } from '../../video/video';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  listVideos:ListVideos;
  videoShowed:Video;
  loading:Boolean;
  finished:Boolean;
  autoplay:Number;
  private firstLoad:Boolean;

  constructor(private videoService:VideoService) {
    this.listVideos = new ListVideos;
    this.videoShowed = new Video;
    this.firstLoad = true;
}

  ngOnInit() {
    this.getListVideos();
  }

  private getListVideos(page:String="") {
    this.loading = true
    return this.videoService.getVideos('',page)
      .subscribe(
        listVideos => {
          this.loading = false

          if (!listVideos.videos.length) {
            this.finished = true;
            return;
          }
          if (this.firstLoad) {
            this.videoShowed = listVideos.videos[0];
            this.firstLoad = false;
            this.autoplay = 0;
          }
          this.listVideos.videos.push(...listVideos.videos);
          this.listVideos.nextPage = listVideos.nextPage;
        }
      )
  }

  loadMore() {
    if (this.finished ) {
      return;
    }
    this.getListVideos(this.listVideos.nextPage);
  }

  showVideo(video) {
    if (this.videoShowed.id === video.id) {
      return;
    }
    this.videoShowed = video;
    this.autoplay = 1;
  }
}
