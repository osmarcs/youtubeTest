import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { VideoService } from '../../video/video.service';
import { ListVideos } from '../../video/list-videos';
import { Video } from '../../video/video';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.sass']
})
export class SearchComponent implements OnInit {

  constructor(private route: ActivatedRoute, private videoService: VideoService) { }
  private searchText:String;
  listVideos:ListVideos;
  videoShowed:Video;
  loading:Boolean;
  finished:Boolean;
  autoplay:Number;
  firstLoad:Boolean;

  ngOnInit() {
    this.route.queryParams
    .subscribe(
      params => {
        this.searchText = params['q'];
        this.searchVideos();
      }
    )
  }


  private searchVideos(page:String="") {
    let searchText = this.searchText;
    this.loading = true
    return this.videoService.getVideos(searchText, page)
      .subscribe(
        listVideos => {
          this.loading = false

          if (!listVideos.videos.length) {
            this.finished = true;
            return;
          }

          this.listVideos.videos.push(...listVideos.videos);
          this.listVideos.nextPage = listVideos.nextPage;
        }
      )
  }

  loadMore() {
    if (this.finished) {
      return;
    }
    this.searchVideos(this.listVideos.nextPage);
  }

  showVideo(video) {
    if (this.videoShowed.id === video.id) {
      return;
    }
    this.videoShowed = video;
    this.autoplay = 1;
  }



}
