import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  itemMenus = [];
  constructor() {}

  ngOnInit() {
    this.itemMenus = [
      {title: 'Destaques', url:'/',icon: 'icon-star'},
      {title: 'Vídeos', url:'/videos', icon: 'icon-play'},
    ]
  }


}
