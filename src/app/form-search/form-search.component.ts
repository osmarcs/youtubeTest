import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'form-search',
  templateUrl: './form-search.component.html',
  styleUrls: ['./form-search.component.sass']
})
export class FormSearchComponent implements OnInit {

  active:Boolean = false;
  searchText:String = "";
  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  onBlur() {
    this.active = false
  }

  onFocus() {
    this.active = true;
  }

  onSearch() {
    if (this.searchText.length < 3) {
      return;
    }
    this.router.navigate(['/search'], {queryParams: {q: this.searchText} })
  }

}
