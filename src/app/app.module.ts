import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { VideoModule } from './video/video.module';

import { AppComponent } from './app.component';
import { MenuXptoComponent } from './menu/menu-xpto/menu-xpto.component';
import { SafePipe } from './safe.pipe';
import { FormSearchComponent } from './form-search/form-search.component';

import { HomeComponent } from './pages/home/home.component';
import { VideosComponent } from './pages/videos/videos.component';
import { SearchComponent } from './pages/search/search.component';


const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'videos', component: VideosComponent },
  { path: 'search', component: SearchComponent },
];


@NgModule({
  declarations: [
    AppComponent,
    MenuXptoComponent,
    SafePipe,
    FormSearchComponent,
    HomeComponent,
    VideosComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    VideoModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
