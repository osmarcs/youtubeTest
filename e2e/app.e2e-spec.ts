import { VagasTestPage } from './app.po';

describe('vagas-test App', () => {
  let page: VagasTestPage;

  beforeEach(() => {
    page = new VagasTestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
